﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArmNumbers
{

	class DicNumbers
	{

		public string CreateTextNumber(int number)
		{
			string result = "";
			int countDigit = number.ToString().Length;

			switch (countDigit)
			{
				case 1:
					result = CreateTextNumberTwoDigit(number);
					break;
				case 2:
					result = CreateTextNumberTwoDigit(number);
					break;
				case 3:
					result = CreateTextNumberTreeDigit(number);
					break;
				case 4:
					result = CreateTextNumberFourDigit(number);
					break;
				case 5:
					result = CreateTextNumberFiveDigit(number);
					break;
				case 6:
					result = CreateTextNumberSixDigit(number);
					break;
				case 7:
					result = CreateTextNumberSevenDigit(number);
					break;
				case 8:
					result = CreateTextNumberEighthDigit(number);
					break;
				case 9:
					result = CreateTextNumberNineDigit(number);
					break;

				default:
					Console.WriteLine("Default case");
					break;
			}
			return result;

		}

		public string CreateTextNumberTwoDigit(int number)
		{
			var dicNumber = CreateKeyNumbers();

			string textnumber = "";

			if (dicNumber.TryGetValue(number, out string value))
			{
				textnumber = value;
				return textnumber;
			}



			int k = 1;
			while (number != 0)
			{

				int digit = number % 10;
				int currentkey = k * digit;
				string textdigit = dicNumber[currentkey];
				//textnumber += textdigit;
				textnumber = textnumber.Insert(0, textdigit);
				number /= 10;
				k *= 10;

				int twoDigit = k * number;
				if (dicNumber.TryGetValue(twoDigit, out string value1))
				{
					//textnumber += value1;
					textnumber = textnumber.Insert(0, value1);
					return textnumber;
				}

			}
			return textnumber;
		}

		public string CreateTextNumberTreeDigit(int number)
		{
			var dicNumber = CreateKeyNumbers();


			int digit = number / 100;
			string textnumber = dicNumber[digit] + " ";
			textnumber += dicNumber[100] + " ";
			int newnumber = number - digit * 100;

			if (newnumber == 0)
			{
				return textnumber;
			}
			else
			{
				string newtext = CreateTextNumberTwoDigit(newnumber);
				textnumber += newtext;

			}
			return textnumber;
		}

		public string CreateTextNumberFourDigit(int number)
		{
			var dicNumber = CreateKeyNumbers();


			int digit = number / 1000;
			string textnumber = dicNumber[digit] + " ";
			textnumber += dicNumber[1000] + " ";
			int newnumber = number - digit * 1000;

			if (newnumber == 0)
			{
				return textnumber;
			}
			else
			{
				string newtext = CreateTextNumberTreeDigit(newnumber);
				textnumber += newtext;

			}
			return textnumber;
		}

		public string CreateTextNumberFiveDigit(int number)
		{
			var dicNumber = CreateKeyNumbers();


			int digit = number / 1000;
			string textnumber = CreateTextNumberTwoDigit(digit) + " ";
			textnumber += dicNumber[1000] + " ";
			int newnumber = number - digit * 1000;

			if (newnumber == 0)
			{
				return textnumber;
			}
			else
			{
				if ((newnumber / 100) == 0)
				{
					string newtext = CreateTextNumberTwoDigit(newnumber);
					textnumber += newtext;
				}
				else
				{
					string newtext = CreateTextNumberTreeDigit(newnumber);
					textnumber += newtext;
				}


			}
			return textnumber;
		}

		public string CreateTextNumberSixDigit(int number)
		{
			var dicNumber = CreateKeyNumbers();



			int digit = number / 1000;
			string textnumber = CreateTextNumberTreeDigit(digit) + " ";
			textnumber += dicNumber[1000] + " ";
			int newnumber = number - digit * 1000;

			if (newnumber == 0)
			{
				return textnumber;
			}
			else
			{
				if (newnumber / 100 == 0)
				{
					string newtext = CreateTextNumberTwoDigit(newnumber);
					textnumber += newtext;
				}
				else
				{
					string newtext = CreateTextNumberTreeDigit(newnumber);
					textnumber += newtext;
				}


			}
			return textnumber;
		}

		public string CreateTextNumberSevenDigit(int number)
		{
			var dicNumber = CreateKeyNumbers();


			int digit = number / 1000000;
			string textnumber = CreateTextNumberTwoDigit(digit) + " ";
			textnumber += dicNumber[1000000] + " ";
			int newnumber = number - digit * 1000000;

			if (newnumber == 0)
			{
				return textnumber;
			}
			else
			{
				string newtext = CreateTextNumberSixDigit(newnumber);
				textnumber += newtext;

			}
			return textnumber;
		}

		public string CreateTextNumberEighthDigit(int number)
		{

			var dicNumber = CreateKeyNumbers();


			int digit = number / 1000000;
			string textnumber = CreateTextNumberTwoDigit(digit) + " ";
			textnumber += dicNumber[1000000] + " ";
			int newnumber = number - digit * 1000000;

			if (newnumber == 0)
			{
				return textnumber;
			}
			else
			{
				string newtext = CreateTextNumberSixDigit(newnumber);
				textnumber += newtext;

			}
			return textnumber;
		}

		public string CreateTextNumberNineDigit(int number)
		{
			var dicNumber = CreateKeyNumbers();


			int digit = number / 1000000;
			string textnumber = CreateTextNumberTreeDigit(digit) + " ";
			textnumber += dicNumber[1000000] + " ";
			int newnumber = number - digit * 1000000;

			if (newnumber == 0)
			{
				return textnumber;
			}
			else
			{

				string newtext = CreateTextNumberSixDigit(newnumber);
				textnumber += newtext;

			}
			return textnumber;
		}

		public Dictionary<double, string> CreateKeyNumbers()
		{
			var numbers = new Dictionary<double, string>();
			numbers.Add(0, "zro");
			numbers.Add(1, "mek");
			numbers.Add(2, "erku");
			numbers.Add(3, "ereq");
			numbers.Add(4, "chors");
			numbers.Add(5, "hing");
			numbers.Add(6, "vec");
			numbers.Add(7, "yot");
			numbers.Add(8, "ut");
			numbers.Add(9, "inny");
			numbers.Add(10, "tasy");
			numbers.Add(20, "qsan");
			numbers.Add(30, "eresun");
			numbers.Add(40, "qarasun");
			numbers.Add(50, "hisun");
			numbers.Add(60, "vacun");
			numbers.Add(70, "yotanasun");
			numbers.Add(80, "utanasun");
			numbers.Add(90, "innsun");
			numbers.Add(100, "haryur");
			numbers.Add(1000, "hazar");
			numbers.Add(1000000, "milion");
			numbers.Add(1000000000, "miliard");


			return numbers;
		}

	}


}
