﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArmNumbers
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.Write("Please Enter any number=");
			int number = Convert.ToInt32(Console.ReadLine());

			DicNumbers numb = new DicNumbers();
			string str = numb.CreateTextNumber(number);

			Console.WriteLine($"Name in Armanian ` {str}");
			Console.ReadLine();
		}
	}
}
